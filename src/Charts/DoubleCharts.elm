module Charts.DoubleCharts exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Events exposing (onClick)
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill)
import TypedSvg.Attributes.InPx exposing (x, y)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Events exposing (onDblClick )
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Array
import Array2D
import List.Extra

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (ChartModel, PrzTableCell)
import SharingStyle exposing (lineChartStyle, barsStyle, stackStyle)
import SharingView exposing ( drawLine, dataOnHover )
import Util exposing
  ( legendIsMiddle
  , formatData
  , lineFormatData
  , getLineDataBody
  , getLinesColor
  , getAbscAndVAlue
  , getAbscissesAndColors
  , column
  , getMin
  , getMax
  )




doubleChartsView : ChartModel -> Svg Msg
doubleChartsView model =
    let
        ( listColors, abscisses ) =
            getAbscissesAndColors  model.doubleChart.dataInBar

        data = lineFormatData model
        colorsList = getLinesColor model
        dataFormatted = List.map2 Tuple.pair colorsList data
        abscAndVAlue = getAbscAndVAlue model
        dataBody = getLineDataBody model

        transX = Transition.value model.doubleChart.transitionX
        transWidth = Transition.value model.doubleChart.transitionWidth

        sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)

        sub2 =( model.height - ( Scale.convert (yScale2 model) 0) - (2 * model.padding.top)) - (model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top))
    in
    g []
        [ barsStyle listColors model
        , lineChartStyle  model dataBody
        , if model.barChart.stacked then
            stacked model (Shape.stack <| config model.doubleChart.dataInBar)
          else
            g [ transform [ Translate model.padding.top model.padding.top ], TypedSvg.Attributes.class [ "series" ] ]
              (List.map (column model (xScale model abscisses)) abscisses) -- bar Chart

        , g [ transform [ Translate model.padding.top model.padding.top ] ]
            (List.map (drawLine model) dataFormatted ) -- curve Chart

        , g [] <| List.map (\r -> g [] <| List.map (\(x, y) -> dataOnHover model (x ,(Tuple.first y + model.padding.top, Tuple.second y + model.padding.top)) ) r )  abscAndVAlue
        , g []
            [ rect
                [ TypedSvg.Attributes.InPx.x <| transX
                , TypedSvg.Attributes.InPx.y <| model.padding.top - 5 + (if legendIsMiddle model then -model.padding.top else 0)
                , TypedSvg.Attributes.InPx.width <| transWidth
                , TypedSvg.Attributes.InPx.height <| model.height - (model.padding.top * 2) + 10 + (if legendIsMiddle model then model.padding.top else 0)
                , TypedSvg.Attributes.style ("fill : " ++ model.settings.backgroundColor)
                ] []
            ]
        , if model.barChart.stacked then
            g [ transform [ Translate (model.padding.left - 1) (model.height - model.padding.bottom - sub) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
                [ xAxisStacked model]
          else
            g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top - sub) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
                [ xAxis model abscisses ]
        , g [ transform [ Translate (model.padding.top - 1) (model.padding.top) ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
            [ yAxis model ]
        , if legendIsMiddle model then
              g [ transform [ Translate ( model.width - model.padding.top * 6 - 1) (model.padding.top + sub2 ) ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
                  [ yAxis2 model ]
          else
              g [ transform [ Translate ( model.width - model.padding.top * 2 - 1) (model.padding.top + sub2) ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
                  [ yAxis2 model ]
        ]



stacked: ChartModel -> StackResult String  -> Svg Msg
stacked model { values, labels, extent } =
    let
        negativeConfig =
          nConfig model.doubleChart.dataInBar
          |> Shape.stack

        nLabels =  negativeConfig.labels
        nYearValues = List.Extra.transpose <| negativeConfig.values

        headerWithColor : List String -> List (List PrzTableCell)
        headerWithColor labelList = List.map (\l->
                              List.map (\hd -> if l == hd.name then hd else PrzTableCell "" "")  getHeaderRow
                              ) labelList

        getHeaderRow =  List.drop 1 <| List.map (\d ->  Maybe.withDefault (PrzTableCell "" "") (List.head d)) model.displayData

        yearValues =
            List.Extra.transpose values

        abscisses =
            listOfAbscisses model.displayData

        scaledValues =
            List.map
                (
                  List.map (\( y1, y2 ) ->
                                ( Scale.convert (yScale model) y1, Scale.convert (yScale model) y2 )
                            )
                ) yearValues

        nScaledValues =
            List.map
                (
                  List.map (\( y1, y2 ) ->
                                ( Scale.convert (yScale model) y1, Scale.convert (yScale model) y2 )
                            )
                ) nYearValues

        ( listColors, absc ) =
            getAbscissesAndColors model.displayData

        t =
            Transition.value model.barChart.transition

        sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)

        nbRow =
          scaledValues
          |> List.head
          |> Maybe.withDefault []
          |> List.length
    in
    g []
        [ stackStyle abscisses nbRow model
        , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
            List.map (stackedCol True model (headerWithColor labels) (xScaleStacked model)) (List.map2 (\a b -> ( a, b )) abscisses scaledValues)
        , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
            List.map (stackedCol False model (headerWithColor nLabels) (xScaleStacked model )) (List.map2 (\a b -> ( a, b )) abscisses nScaledValues)
        ]


stackedCol : Bool -> ChartModel -> List (List PrzTableCell) -> BandScale String -> (String, List ( Float, Float ))  ->  Svg Msg
stackedCol isPositiveVal model headerOrdered bScale ( year, values )  =
    let
        getColors =
          List.map (\x -> "#"++x)
          <| String.split "#"
          <| String.dropLeft 1
          <| String.join ""
          <| List.concat
          <| List.map (\i -> List.map (\j -> j.color) i) headerOrdered

        getColIndex =
          model.displayData
          |> List.head
          |> Maybe.withDefault []
          |> List.map .name
          |> List.indexedMap Tuple.pair
          |> List.map (\(i, cell)-> if cell == year then (String.fromInt i) else "")
          |> String.concat
          |> String.toInt
          |> Maybe.withDefault 0

        block : (Int, String) -> (Float, Float) -> Svg Msg
        block (row, c) ( upperY, lowerY ) =
          let
            color =  hexToColor c |> Result.withDefault Color.gray

            getCell =
              model.displayData
              |> Array2D.fromList
              |> .data
              |> Array.get (row + 1)
              |> Maybe.withDefault (Array.fromList [])
              |> Array.get getColIndex
              |> Maybe.withDefault (PrzTableCell "" "")
              |> .name

            (val ,simple) = formatData getCell model.settings

            (defaultVal, simpleVal) =
              if String.contains "-" getCell then
                ("-"++val, "-"++simple)
              else
                (val ,simple)
          in
          g [TypedSvg.Attributes.class [ "stack" ++ year ++  (String.fromInt row)], onDblClick <| Stacked (not model.barChart.stacked)]
            [ rect
                  [ x <| Scale.convert bScale (year)
                  , y <| if isPositiveVal then lowerY else upperY
                  , TypedSvg.Attributes.InPx.width <| Scale.bandwidth bScale
                  , TypedSvg.Attributes.InPx.height <| (abs <| upperY - lowerY)
                  , fill <| Paint color
                  ]
                  []
            , text_
              [ x <| Scale.convert bScale (year) + (Scale.bandwidth bScale /2 ) - 25
              , y <| 0
              -- , textAnchor AnchorMiddle
              , TypedSvg.Attributes.class [ "data-value" ]
              ]
              [text <| if model.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
            ]

    in
    g [ TypedSvg.Attributes.class [ "column" ] ]
      (List.map2 block (List.indexedMap Tuple.pair getColors) values)



------------------------------ Axis function ----------------------------------

xScaleStacked : ChartModel -> BandScale String
xScaleStacked model  =
  let
    abscisses =
        listOfAbscisses model.displayData
  in
  abscisses
    |> Scale.band
        { defaultBandConfig | paddingInner = 0.1, paddingOuter = 0.2 }
        -- ( 0, model.width - (model.padding.top + model.padding.bottom) - (if legendIsMiddle model  then 140 else 0))
        ( 0 , model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top - 50 )

xAxisStacked : ChartModel -> Svg msg
xAxisStacked model =
  Axis.bottom [ Axis.tickCount 5 ] (Scale.toRenderable (\s -> s) (xScaleStacked model ))


xAxis : ChartModel -> List ( String, List String ) -> Svg msg
xAxis model data =
    Axis.bottom [] (Scale.toRenderable identity (xScale model data))


xScale : ChartModel -> List ( String, List String ) -> BandScale String
xScale model data =
    let
        ( str, innerList ) =
            List.head data
                |> Maybe.withDefault ( "", [] )

        x =
            toFloat <| List.length innerList

        mydata = data ++ [("", [])]

        padI =
            1 - (1 / (x + 1))
    in
    List.map Tuple.first mydata
        |> Scale.band { defaultBandConfig | paddingInner = padI, paddingOuter = 0.1 }
            ( 0 , model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top - 50 )


yScale : ChartModel -> ContinuousScale Float
yScale model =
    let
      maxBarAxe = getMax model.doubleChart.dataInBar
      minBarAxe = getMin model.doubleChart.dataInBar

      negativeConfig =
        nConfig model.doubleChart.dataInBar
        |> Shape.stack

      positiveConfig =
        config model.doubleChart.dataInBar
        |> Shape.stack

      min =
        if model.barChart.stacked then
          Tuple.first negativeConfig.extent
        else
          if model.minHeightScale >= 0 then 0 else model.minHeightScale

      max =
        if model.barChart.stacked then
          (Tuple.second positiveConfig.extent)
        else
          model.maxHeightScale


      -- Tuple.first negativeConfig.extent , Tuple.second extent
    in
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50

          else
            0
        )
        ( min,max)


yAxis : ChartModel -> Svg msg
yAxis model =
    Axis.left [ Axis.tickCount 5 ] (yScale model)

yScale2 : ChartModel -> ContinuousScale Float
yScale2 model =
    let
      maxCurveAxe = getMax  model.doubleChart.dataInCurve
      minCurveAxe = getMin  model.doubleChart.dataInCurve

      negativeConfig =
        nConfig model.doubleChart.dataInBar
        |> Shape.stack

      positiveConfig =
        config model.doubleChart.dataInBar
        |> Shape.stack

      min =
        if model.barChart.stacked then
          if minCurveAxe < Tuple.first negativeConfig.extent then
            minCurveAxe
          else
            Tuple.first negativeConfig.extent
        else
          if model.minHeightScale >= 0 then 0 else model.minHeightScale

      max =
        if model.barChart.stacked then
          if maxCurveAxe > Tuple.second positiveConfig.extent then
            maxCurveAxe
          else
            Tuple.second positiveConfig.extent
        else
          model.maxHeightScale

    in
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50
          else
            0
        )
        ( min, max)

yAxis2 : ChartModel -> Svg msg
yAxis2 model =
    Axis.right [ Axis.tickCount 5 ] (yScale2 model)


listOfAbscisses : List (List PrzTableCell) -> List String
listOfAbscisses data =
  List.head data
  |> Maybe.withDefault []
  |> List.drop 1
  |> List.map .name
  -- List.map .year crimeRates

config : List (List PrzTableCell) -> StackConfig String
config d =
   { data = (dataRows d)
   , offset = Shape.stackOffsetNone
   , order =
        -- stylistic choice: largest (by sum of values) category at the bottom
        -- List.sortBy (Tuple.second >> List.sum >> negate)
       List.map (\r-> r)
   }

nConfig : List (List PrzTableCell) -> StackConfig String
nConfig d =
   { data = (nDataRows d)
   , offset = Shape.stackOffsetNone
   , order =
        -- stylistic choice: largest (by sum of values) category at the bottom
        -- List.sortBy (Tuple.second >> List.sum >> negate)
       List.map (\r-> r)
   }



dataRows : List (List PrzTableCell) -> List (String, List Float)
dataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) <= 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )

nDataRows : List (List PrzTableCell) -> List (String, List Float)
nDataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) > 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )
