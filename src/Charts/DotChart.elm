module Charts.DotChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Events exposing (onClick)
import Array
import Shape
import Axis
import Path exposing (Path)
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import TypedSvg exposing (svg, g, text_, rect)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Attributes exposing (stroke, fill, transform, viewBox, textAnchor )
import TypedSvg.Attributes.InPx exposing (strokeWidth, height, width, x, y)
import TypedSvg.Events exposing (onLoad)
import TypedSvg.Types exposing (Paint(..), Transform(..), AnchorAlignment(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (ChartModel)
import SharingStyle exposing (svgChartStyle, dotChartStyle, barsStyle)
import SharingView exposing (drawLine)
import Util exposing ( getAbscissesAndColors, legendIsMiddle, formatData, convertDataToFloat)


dotChartView : ChartModel -> Svg msg
dotChartView model =
    let
        ( listColors, abscisses ) =
            getAbscissesAndColors model.data

        transX =  Transition.value model.curveChart.transitionX
        transWidth =  Transition.value model.curveChart.transitionWidth

        sub =  model.height - ( Scale.convert (yScale model) 0) - (2 * model.padding.top)
    in
    g []
        [ barsStyle listColors model
        , g [ transform [ Translate model.padding.top model.padding.top ], TypedSvg.Attributes.class [ "series" ] ]
            (List.map (point model (xScale model abscisses)) abscisses)
        , g []
            [ rect
                [ TypedSvg.Attributes.InPx.x <| transX  --model.padding.top + 2
                , TypedSvg.Attributes.InPx.y <|  model.padding.top - 5 + (if legendIsMiddle model  then -50 else 0)
                , TypedSvg.Attributes.InPx.width <| transWidth
                , TypedSvg.Attributes.InPx.height <| model.height - (model.padding.top * 2) + 10 + (if legendIsMiddle model  then 50 else 0)
                , TypedSvg.Attributes.style ("fill : " ++ model.settings.backgroundColor)
                ] []
            ]
        , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top - sub) ], TypedSvg.Attributes.class [ "svg-axes-x" ] ]
            [ xAxis model abscisses ]
        , g [ transform [ Translate (model.padding.top - 1) model.padding.top ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
            [ yAxis model ]

        ]


point : ChartModel -> BandScale String -> ( String, List String ) -> Svg msg
point tmodel bscale ( date, listNW ) =
    let
      sub =  tmodel.height - ( Scale.convert (yScale tmodel) 0) - (2 * tmodel.padding.top)

      funcColumn : ( Int, String ) -> Svg msg
      funcColumn ( i, str ) =
          let
              (sci, simpleData) = formatData str tmodel.settings

              val =
                if String.contains "-" str then
                (Maybe.withDefault 0 <| String.toFloat ("-" ++ String.fromFloat (convertDataToFloat sci tmodel.settings)))
                else
                  convertDataToFloat sci tmodel.settings

              (defaultVal, simpleVal) =
                if String.contains "-" str then
                  ("-"++sci, "-"++simpleData)
                else
                  (sci, simpleData)

              scaleConv =
                  if val < 0 then
                    (Scale.convert (yScale tmodel) 0  + (tmodel.height - (Scale.convert (yScale tmodel) val) - (2 * tmodel.padding.top)))
                  else
                    Scale.convert (yScale tmodel) val

              xx = Scale.convert bscale date + (Scale.bandwidth bscale * toFloat i) + 2
              yy = Scale.convert (yScale tmodel) val

          in
          g [ TypedSvg.Attributes.class [ "column" ++ String.fromInt (i + 1) ] ]
              [ Path.element circle [ fill <| Paint <| (hexToColor tmodel.dotChart.fillColor |> Result.withDefault Color.gray)
                                    , stroke <| Paint <| (hexToColor tmodel.dotChart.borderColor |> Result.withDefault Color.gray)
                                    , transform [ Translate xx yy ]
                                    ]
              , text_
                  [ x <| xx + 30
                  , y <| yy
                  , textAnchor AnchorMiddle
                  , TypedSvg.Attributes.class [ "data-value" ]
                  ]
                  [ TypedSvg.Core.text <| if tmodel.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
              ]
    in
    g []
        (List.map funcColumn (List.indexedMap Tuple.pair listNW))



circle : Path
circle =
    Shape.arc
        { innerRadius = 0
        , outerRadius = 3
        , cornerRadius = 0
        , startAngle = 0
        , endAngle = 2 * pi
        , padAngle = 0
        , padRadius = 0
        }


xAxis : ChartModel -> List ( String, List String ) -> Svg msg
xAxis model data =
    Axis.bottom [] (Scale.toRenderable identity (xScale model data))


xScale : ChartModel -> List ( String, List String ) -> BandScale String
xScale model data =
    let
        ( str, innerList ) =
            List.head data
                |> Maybe.withDefault ( "", [] )

        x =
            toFloat <| List.length innerList

        mydata = data ++ [("", [])]

        padI =
            1 - (1 / (x + 1))
    in
    List.map Tuple.first mydata
        |> Scale.band { defaultBandConfig | paddingInner = padI, paddingOuter = 0.1 }
            ( 0 , model.width - (if legendIsMiddle model  then 6 else 2 ) * model.padding.top )


yScale : ChartModel -> ContinuousScale Float
yScale model =
    -- Scale.linear ( model.height - 2 * model.padding.top, 0 ) ( 0, model.maxHeightScale )
    Scale.linear
        ( model.height - 2 * model.padding.top
        , if legendIsMiddle model  then
            -50

          else
            0
        )
        ( model.minHeightScale, model.maxHeightScale )


yAxis : ChartModel -> Svg msg
yAxis model =
    Axis.left [ Axis.tickCount 5 ] (yScale model)
