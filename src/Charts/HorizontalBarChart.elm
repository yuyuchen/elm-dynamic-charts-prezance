module Charts.HorizontalBarChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Axis
import Scale exposing (BandScale, ContinuousScale, defaultBandConfig)
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, text_, svg)
import TypedSvg.Attributes exposing (class, textAnchor, dominantBaseline, transform, viewBox, fill)
import TypedSvg.Attributes.InPx exposing (x, y)
import TypedSvg.Events exposing (onLoad, onDblClick)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (AnchorAlignment(..), DominantBaseline(..), Transform(..), Paint(..))
import Color exposing (Color)
import Color.Convert exposing (hexToColor)
import Transition exposing (Transition)
import Json.Decode as Decode exposing (at)
import List.Extra exposing (uncons)
import Array
import Array2D

-- Prezance packages --
import Update exposing (Msg(..))
import Model exposing (ChartModel, PrzTableCell)
import SharingStyle exposing (barsStyle, svgChartStyle, stackStyle)
import Util exposing ( getAbscissesAndColors, column, legendIsMiddle, formatData, convertDataToFloat)



horizontalBarChartView : ChartModel -> Svg Msg
horizontalBarChartView model =
    let
        ( listColors, abscisses ) =
            getAbscissesAndColors model.displayData

        sub =  ( Scale.convert (xScale model) 0)

        nbRow =
          abscisses
          |> List.length

        transX =  Transition.value model.curveChart.transitionX
        transWidth =  Transition.value model.curveChart.transitionWidth
    in
    g []
        [ barsStyle listColors model
        , g [ transform [ Translate model.padding.top model.padding.top ], TypedSvg.Attributes.class [ "series" ] ]
            (List.map (column model (yScale model abscisses)) abscisses)
        , g []
            [ rect
                [ TypedSvg.Attributes.InPx.x <| transX  --model.padding.top + 2
                , TypedSvg.Attributes.InPx.y <|  model.padding.top - 5 + (if legendIsMiddle model  then -50 else 0)
                , TypedSvg.Attributes.InPx.width <| transWidth
                , TypedSvg.Attributes.InPx.height <| model.height - (model.padding.top * 2) + 10 + (if legendIsMiddle model  then 50 else 0)
                , TypedSvg.Attributes.style ("fill : " ++ model.settings.backgroundColor)
                ] []
            ]
        , g [ transform [ Translate (model.padding.top - 1 + sub) model.padding.top ], TypedSvg.Attributes.class [ "svg-axes-y" ] ]
            [ yAxis model abscisses ]
        , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top ) ], TypedSvg.Attributes.class [ "svg-axes-x" ] ]
            [ xAxis model ]
        ]

horizontalStackedView : ChartModel -> StackResult String  -> Svg Msg
horizontalStackedView model { values, labels, extent } =
  let

    ( listColors, abscisses ) = getAbscissesAndColors model.displayData
    date = listOfAbscisses model.displayData
    sub =  ( Scale.convert (xScaleT model) 0)

    headerWithColor : List String -> List (List PrzTableCell)
    headerWithColor labelList = List.map (\l->
                          List.map (\hd -> if l == hd.name then hd else PrzTableCell "" "")  getHeaderRow
                          ) labels

    getHeaderRow =  List.drop 1 <| List.map (\d ->  Maybe.withDefault (PrzTableCell "" "") (List.head d)) model.displayData

    negativeConfig = nHorizontalConfig model.displayData |> Shape.stack
    nLabels =  negativeConfig.labels
    nYearValues = List.Extra.transpose <| negativeConfig.values
    nScaledValues =
        List.map
            (
              List.map (\( y1, y2 ) ->
                            ( Scale.convert (xScaleT model) y1, Scale.convert (xScaleT model) y2 )
                        )
            ) nYearValues

    yearValues = List.Extra.transpose values
    scaledValues =
        List.map
            (
              List.map (\( y1, y2 ) ->
                            ( Scale.convert (xScaleT model) y1, Scale.convert (xScaleT model) y2 )
                        )
            ) yearValues

    nbRow =
      scaledValues
      |> List.head
      |> Maybe.withDefault []
      |> List.length


    transX =  Transition.value model.curveChart.transitionX
    transWidth =  Transition.value model.curveChart.transitionWidth
  in
  g []
      [ stackStyle date nbRow model
      , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
          List.map (stackedColumn True model (headerWithColor labels) (yScale model abscisses)) (List.map2 (\a b -> ( a, b )) date scaledValues)
      , g [ transform [ Translate model.padding.left model.padding.top ], TypedSvg.Attributes.class [ "series" ] ] <|
          List.map (stackedColumn False model (headerWithColor nLabels) (yScale model abscisses)) (List.map2 (\a b -> ( a, b )) date nScaledValues)
      , g []
          [ rect
              [ TypedSvg.Attributes.InPx.x <| transX  --model.padding.top + 2
              , TypedSvg.Attributes.InPx.y <|  model.padding.top - 5 + (if legendIsMiddle model  then -50 else 0)
              , TypedSvg.Attributes.InPx.width <| transWidth
              , TypedSvg.Attributes.InPx.height <| model.height - (model.padding.top * 2) + 10 + (if legendIsMiddle model  then 50 else 0)
              , TypedSvg.Attributes.style ("fill : " ++ model.settings.backgroundColor)
              ] []
          ]
      , g [ transform [ Translate (model.padding.top - 1 + sub) model.padding.top ] , TypedSvg.Attributes.class [ "svg-axes-y" ]]
          [ yAxis model abscisses ]
      , g [ transform [ Translate (model.padding.top - 1) (model.height - model.padding.top ) ] , TypedSvg.Attributes.class [ "svg-axes-x" ]]
          [ xAxisT model]
      ]

stackedColumn : Bool -> ChartModel -> List (List PrzTableCell) -> BandScale String -> (String, List ( Float, Float ))  ->  Svg Msg
stackedColumn isPositiveVal model headerOrdered bScale ( year, values )  =
  let

    -- _ = Debug.log "tesst == " headerOrdered
    getColors =
      List.map (\x -> "#"++x)
      <| String.split "#"
      <| String.dropLeft 1
      <| String.join ""
      <| List.concat
      <| List.map (\i -> List.map (\j -> j.color) i) headerOrdered

    getColIndex =
      model.displayData
      |> List.head
      |> Maybe.withDefault []
      |> List.map .name
      |> List.indexedMap Tuple.pair
      |> List.map (\(i, cell)-> if cell == year then (String.fromInt i) else "")
      |> String.concat
      |> String.toInt
      |> Maybe.withDefault 0

    block : (Int, String) -> (Float, Float) -> Svg Msg
    block (row, c) ( upperY, lowerY ) =
      let
        color =  hexToColor c |> Result.withDefault Color.gray

        -- _ = Debug.log "tesst == " c
        getCell =
          model.displayData
          |> Array2D.fromList
          |> .data
          |> Array.get (row + 1)
          |> Maybe.withDefault (Array.fromList [])
          |> Array.get getColIndex
          |> Maybe.withDefault (PrzTableCell "" "")
          |> .name

        (val ,simple) = formatData getCell model.settings

        (defaultVal, simpleVal) =
          if String.contains "-" getCell then
            ("-"++val, "-"++simple)
          else
            (val ,simple)

        yy = Scale.convert bScale (year) - (rectHeight/ 2) + 2
        xx = if isPositiveVal then upperY else lowerY

        rectHeight = Scale.bandwidth bScale * 3
        rectWidth = (abs <| upperY - lowerY)
      in
      g [TypedSvg.Attributes.class [ "stack" ++ year ++  (String.fromInt row)], onDblClick <| Stacked (not model.barChart.stacked)]
        [ rect
              [ x <| xx
              , y <| yy
              , TypedSvg.Attributes.InPx.width <| rectWidth
              , TypedSvg.Attributes.InPx.height <|  rectHeight
              , fill <| Paint color
              ]
              []
        , text_
          [ x <| xx + rectWidth / 2 - 10
          , y <| yy + rectHeight / 2 + 10
          -- , textAnchor AnchorMiddle
          , TypedSvg.Attributes.class [ "data-value" ]
          ]
          [text <| if model.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
        ]

  in
    g [ TypedSvg.Attributes.class [ "column" ] ]
      (List.map2 block (List.indexedMap Tuple.pair getColors) values)



listOfAbscisses : List (List PrzTableCell) -> List String
listOfAbscisses data =
  List.head data
  |> Maybe.withDefault []
  |> List.drop 1
  |> List.map .name
  -- List.map .year crimeRates

horizontalConfig : List (List PrzTableCell) -> StackConfig String
horizontalConfig d =
   { data = (dataRows d)
   , offset = Shape.stackOffsetNone
   , order = List.map (\r-> r)
   }


nHorizontalConfig : List (List PrzTableCell) -> StackConfig String
nHorizontalConfig d =
   { data = (nDataRows d)
   , offset = Shape.stackOffsetNone
   , order =
        -- stylistic choice: largest (by sum of values) category at the bottom
        -- List.sortBy (Tuple.second >> List.sum >> negate)
       List.map (\r-> r)
   }

dataRows : List (List PrzTableCell) -> List (String, List Float)
dataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) <= 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )

nDataRows : List (List PrzTableCell) -> List (String, List Float)
nDataRows data =
  List.tail data
  |> Maybe.withDefault []
  |> List.map List.Extra.uncons
  |> List.map (Maybe.withDefault (PrzTableCell "" "", []))
  |> List.map (\(a, slist) ->
                    ( a.name , List.map (\tableH -> if (Maybe.withDefault 0 (String.toFloat tableH.name)) > 0 then 0 else (Maybe.withDefault 0 (String.toFloat tableH.name))) slist)
              )

-----------------------------------------------------------------

column : ChartModel -> BandScale String -> ( String, List String ) -> Svg Msg
column tmodel bscale ( date, listNW ) =
    let
        funcColumn : ( Int, String ) -> Svg Msg
        funcColumn ( i, str ) =
            let
                (sci, simpleData) = formatData str tmodel.settings

                val =
                  if String.contains "-" str then
                  (Maybe.withDefault 0 <| String.toFloat ("-" ++ String.fromFloat (convertDataToFloat sci tmodel.settings)))
                  else
                    convertDataToFloat sci tmodel.settings
                (defaultVal, simpleVal) =
                  if String.contains "-" str then
                    ("-"++sci, "-"++simpleData)
                  else
                    (sci, simpleData)

                xx =
                  if val >= 0 then
                    Scale.convert (xScale tmodel) 0
                  else
                    Scale.convert (xScale tmodel) val

                yy = Scale.convert bscale date + (Scale.bandwidth bscale * (toFloat i))

                rectWidth =
                  if val >= 0 then
                    (Scale.convert (xScale tmodel) val) -  ( Scale.convert (xScale tmodel) 0)
                  else
                    ( Scale.convert (xScale tmodel) 0) - (Scale.convert (xScale tmodel) val)

            in
            g [ TypedSvg.Attributes.class [ "column" ++ String.fromInt (i + 1) ], onDblClick <| Stacked (not tmodel.barChart.stacked) ]
                [
                  rect
                  [ x <| xx
                  , y <| yy
                  , TypedSvg.Attributes.InPx.width <| rectWidth
                  , TypedSvg.Attributes.InPx.height <| Scale.bandwidth bscale
                  ]
                  []
                , text_
                    [ x <| if val >= 0 then (xx + rectWidth + tmodel.padding.top / 2) else (xx - tmodel.padding.top / 2)
                    , y <| yy+ 10
                    , textAnchor AnchorMiddle
                    , TypedSvg.Attributes.class [ "data-value" ]
                    ]
                    [ TypedSvg.Core.text <| if tmodel.settings.simpleData && simpleVal /= "" then simpleVal else defaultVal ]
                ]
    in
    g []
        (List.map funcColumn (List.indexedMap Tuple.pair listNW))

---------------------------- Axis function --------------------------------

xScaleT : ChartModel -> ContinuousScale Float
xScaleT   model =
  let
    max =  horizontalConfig model.displayData |> Shape.stack |> .extent |> Tuple.second
    min =  nHorizontalConfig model.displayData |> Shape.stack |> .extent |> Tuple.first

  in
    Scale.linear
        ( 0
        , (model.width - 2 * model.padding.top + 10) - (if legendIsMiddle model then 150 else 0)
        )
        (min * 1.1, max * 1.1 )


xAxisT :  ChartModel -> Svg msg
xAxisT  model =
    Axis.bottom [ Axis.tickCount 5 ] (xScaleT model)


yAxis : ChartModel -> List ( String, List String ) -> Svg msg
yAxis model data =
    Axis.left [] (Scale.toRenderable identity (yScale model data))


yScale : ChartModel -> List ( String, List String ) -> BandScale String
yScale model data =
    let
        ( str, innerList ) =
            List.head data
                |> Maybe.withDefault ( "", [] )
        x =
            toFloat <| List.length innerList

        padI =
            1 - (1 / (x + 1))
    in
    List.map Tuple.first data
        |> Scale.band { defaultBandConfig | paddingInner = padI, paddingOuter = 1 }
            (  model.height - 2  * model.padding.top , (if legendIsMiddle model then -50 else 0) )


xScale : ChartModel -> ContinuousScale Float
xScale model =
    Scale.linear
        ( 0
        , (model.width - 2 * model.padding.top + 10) - (if legendIsMiddle model then 150 else 0)
        )
        (model.minHeightScale, model.maxHeightScale)


xAxis : ChartModel -> Svg msg
xAxis model =
    Axis.bottom [ Axis.tickCount 5 ] (xScale model)
