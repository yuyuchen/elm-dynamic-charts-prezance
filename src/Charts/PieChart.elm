module Charts.PieChart exposing (..)

-- Elm packages --
import Html exposing (..)
import Html.Attributes exposing (..)
import Array exposing (Array)
import Color exposing (Color)
import Path
import Transition exposing (Transition)
import Shape exposing (defaultPieConfig)
import TypedSvg exposing (g, svg, text_)
import TypedSvg.Attributes exposing (dy, fill, stroke, textAnchor, transform, viewBox)
import TypedSvg.Attributes.InPx exposing (height, width)
import TypedSvg.Core exposing (Svg, text)
import TypedSvg.Types exposing (AnchorAlignment(..), Paint(..), Transform(..), em)
import Color.Convert exposing (hexToColor)

-- Prezance packages --
import Update exposing (Msg)
import Model exposing (ChartModel, PrzTableCell)
import Util exposing (getLinesColor, legendIsMiddle, formatData)
import SharingStyle exposing (pieChartStyle)


pieChartView :ChartModel -> Svg msg
pieChartView model=
    let
        radius = Basics.min 990 504 / 2
        formatPieData =
          List.map (\row -> (.name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get 0 <| Array.fromList row
          , Maybe.withDefault 0 <| String.toFloat <| .name <|  Maybe.withDefault (PrzTableCell "" "") <| Array.get (model.pieChart.colName + 1) <| Array.fromList row))
          <| Maybe.withDefault []
          <| List.tail model.displayData



        pieData =
            formatPieData |> List.map Tuple.second |> Shape.pie { defaultPieConfig | outerRadius = radius-150, innerRadius = 0 }

        -- animation
        block = Shape.pie { defaultPieConfig | outerRadius = 0, innerRadius = 260, endAngle = Transition.value model.pieChart.transition1} [0.1]
        animatedArc ind dat=
          Path.element (Shape.arc dat) [fill <| Paint <| (hexToColor model.settings.backgroundColor |> Result.withDefault Color.gray)  ]


        -- draw data value
        makeSlice index datum =
          let
            colorsList = Array.fromList <| getLinesColor model
            getColor = Result.withDefault Color.gray <| hexToColor <| Maybe.withDefault "" <| Array.get index colorsList

            ( x, y ) =
                Shape.centroid { datum | innerRadius = 0, outerRadius = radius - 150 }

            value = formatPieData |> Array.fromList |> Array.get index |> Maybe.withDefault ("",0)

            formattedData =  Tuple.first <| formatData (value |> Tuple.second |> String.fromFloat) model.settings
            simpleData = Tuple.second <| formatData (value |> Tuple.second |> String.fromFloat) model.settings
          in
          g [ TypedSvg.Attributes.class [ Tuple.first value ] ] [ Path.element (Shape.arc datum) [ fill <| Paint getColor, stroke <| Paint Color.white ]
               , text_
                     [ transform [ Translate x y ]
                     , dy (TypedSvg.Types.em 0.35)
                     , textAnchor AnchorMiddle
                     , TypedSvg.Attributes.class [ "data-value" ]
                     ]
                     [ Html.text  <| if model.settings.simpleData && simpleData /= "" then simpleData else formattedData]
               ]


        -- draw circle
        -- makeLabel slice ( label, value ) =
        --     let
        --         -- _ = Debug.log "tesst = " <|slice
        --
        --         ( x, y ) =
        --             Shape.centroid { slice | innerRadius = 0, outerRadius = radius - 150 }
        --     in
        --     text_
        --         [ transform [ Translate x y ]
        --         , dy (TypedSvg.Types.em 0.35)
        --         , textAnchor AnchorMiddle
        --         ]
        --         [ Html.text <| String.fromFloat value ]

        colName =
          .name
          <| Maybe.withDefault  (PrzTableCell "" "")
          <| Array.get (model.pieChart.colName + 1)
          <| Array.fromList
          <| Maybe.withDefault []
          <| List.head
          <| model.data
    in
    g []
        [ pieChartStyle  model formatPieData
         , g [ transform [ Translate ((model.width) / 2) ((model.height) / 2) ] ]
            [ g [ transform [ Translate -25 0 ]] <| List.indexedMap makeSlice pieData
            -- , g [transform [ Translate -25 0 ]] <| List.map2 makeLabel pieData formatPieData
            , g [ transform [ Translate -25 0 ] ] <|  List.indexedMap animatedArc block
            , g [ transform [ Translate -175 -75 ]] [ text_ [ Html.Attributes.style "fontSize" "1.5em"] [Html.text colName]]


            ]
        ]
