module SharingStyle exposing (..)

-- Elm packages --
import TypedSvg exposing (svg, g, text_, rect)
import TypedSvg.Core exposing (Svg)


-- Prezance packages --
import Model exposing (ChartModel)


-- Global chart style
svgChartStyle : ChartModel -> String
svgChartStyle model =
    let
      globalFontFamily = model.settings.globalFontFamily
    in
    """
    .svg-chart-container { background-color:"""
        ++ model.settings.backgroundColor
        ++ """;}

    .svg-chart-title
    {
      font-weight: bold;
      font-family : """++(if globalFontFamily == "none" then model.settings.titleTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.titleTxtSettings.fontSize++"""px;
      fill : """++model.settings.titleTxtSettings.fontColor++""";
      direction: """ ++ (if String.contains "right" model.settings.titlePosition then "rtl" else "ltr")++ """;
    }

    .svg-chart-source
    {
      font-family : """++(if globalFontFamily == "none" then model.settings.sourceTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.sourceTxtSettings.fontSize++"""px;
      fill : """++model.settings.sourceTxtSettings.fontColor++""";
      direction: """ ++ (if String.contains "right" model.settings.sourcePosition then "rtl" else "ltr")++ """;
    }

    .svg-chart-legend
    {
      font-family : """++(if globalFontFamily == "none" then model.settings.legendTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.legendTxtSettings.fontSize++"""px;
      fill : """++(if not model.settings.legendTxtSettings.autoColor then  model.settings.legendTxtSettings.fontColor else "")++""";

    }


    .data-value
    {
      font-family : """++(if globalFontFamily == "none" then model.settings.valueTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.valueTxtSettings.fontSize++"""px;
      fill : """++model.settings.valueTxtSettings.fontColor++""";
      visibility : """++(if model.settings.showValueOnHover then "visible" else "hidden")++""";
    }

    .axe-y
    {
      transform-origin : center;
      transform : rotate(-90deg);
      font-family : """++(if globalFontFamily == "none" then model.settings.axeYTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.axeYTxtSettings.fontSize++"""px;
      fill : """++model.settings.axeYTxtSettings.fontColor++""";
    }
    .svg-axes-y text
    {
      font-family : """++(if globalFontFamily == "none" then model.settings.axeYTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.axeYTxtSettings.fontSize++"""px;
      fill : """++model.settings.axeYTxtSettings.fontColor++""";

    }

    .axe-x, .svg-axes-x text
    {
      font-family : """++(if globalFontFamily == "none" then model.settings.axeXTxtSettings.fontFamily else globalFontFamily)++""";
      font-size : """++String.fromInt model.settings.axeXTxtSettings.fontSize++"""px;
      fill : """++model.settings.axeXTxtSettings.fontColor++""";
    }

    .axe-x, .axe-y
    {
      visibility : """++(if model.settings.showAxes then "visible" else "hidden")++""";
    }

    .legend-item
    { cursor:pointer;

    }


  """



-- Pie Chart
pieChartStyle : ChartModel -> List (String, Float) -> Svg msg
pieChartStyle model data=
  let
      dataHover =
        String.concat <| List.map (\r ->
           """ .""" ++ Tuple.first r ++ """ text { display: none; }
              .""" ++  Tuple.first r ++ """:hover text { display: inline; }
              .""" ++ Tuple.first r ++ """:hover path { opacity: 0.7;  }
          """) data

  in
    TypedSvg.style []
        [ TypedSvg.Core.text <| dataHover ]



-- Curve Chart
lineChartStyle : ChartModel -> List (List Float) -> Svg msg
lineChartStyle model data=
  let
      dataHover =
        String.concat <| List.map (\r -> String.concat <| List.map (\i->
           """ .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """ text { display: none; }
              .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """:hover text { display: inline; }
              .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """:hover circle { width:100px }
          """
           ) r) data
  in
    TypedSvg.style []
        [ TypedSvg.Core.text <| dataHover ]


dotChartStyle : ChartModel -> List (List Float) -> Svg msg
dotChartStyle model data=
  let
      dataHover =
        String.concat <| List.map (\r -> String.concat <| List.map (\i->
           """ .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """ text { display: none; }
              .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """:hover text { display: inline; }
              .point""" ++ String.replace "." ""  (String.fromFloat (i)) ++ """:hover circle { width:100px }
          """
           ) r) data
  in
    TypedSvg.style []
        [ TypedSvg.Core.text <| dataHover ]


-- Bar Chart
barsStyle : List String -> ChartModel -> Svg msg
barsStyle listColors model =
    let
        barChartColors =
            List.map
                (\( i, v ) ->
                    """ .column""" ++ String.fromInt i ++ """ rect { fill: """ ++ v ++ """; }
                    .column""" ++ String.fromInt i ++ """ text { display: none; }
                    .column""" ++ String.fromInt i ++ """:hover rect { opacity: 0.7;  }
                    .column""" ++ String.fromInt i ++ """:hover text { display: inline; }
                    .palette""" ++ String.fromInt i ++ """ { fill: """ ++ v ++ """; }
                    .block rect { fill:""" ++ (if model.settings.animated then model.settings.backgroundColor else "transparent")++ """}

                    """
                )
                (List.indexedMap Tuple.pair listColors)
                |> String.concat
    in
    TypedSvg.style []
        [ TypedSvg.Core.text barChartColors ]


stackStyle : List String -> Int -> ChartModel -> Svg msg
stackStyle listAbsc nbRow model =
    let
        stackChartColors =
          listAbsc
          |> List.map (\absc ->
                          List.range 0 (nbRow - 1)
                          |> List.map (\i ->
                                          """  .stack"""++ absc ++ (String.fromInt i)++ """:hover rect { opacity: 0.7; }
                                               .stack"""++ absc ++ (String.fromInt i)++ """:hover text { display: inline; }
                                               .stack"""++ absc ++ (String.fromInt i)++ """ text { display: none; }

                                          """
                                      )
                      )
          |> List.map String.concat
          |> String.concat
    in
    TypedSvg.style []
        [ TypedSvg.Core.text stackChartColors ]


-- Bar Chart
waterfallStyle : List String -> ChartModel -> Svg msg
waterfallStyle listColors model =
    let
        barChartColors =
            List.map
                (\( i, v ) ->
                    """
                    .bar""" ++ String.fromInt i ++ """ text { display: none; }
                    .bar""" ++ String.fromInt i ++ """:hover rect { opacity: 0.7;  }
                    .bar""" ++ String.fromInt i ++ """:hover text { display: inline; }
                    """
                )
                (List.indexedMap Tuple.pair listColors)
                |> String.concat
    in
    TypedSvg.style []
        [ TypedSvg.Core.text barChartColors ]
