module Main exposing (..)

-- Elm packages --
import Browser exposing (Document)
import Html exposing (..)
import Html.Attributes exposing (..)
import Browser.Events
import Transition exposing (Transition)


-- Prezance packages --
import Model exposing (ChartModel, initialChartModel)
import Update exposing (update, Msg(..))
import View exposing (view)

main : Program () ChartModel Msg
main =
    Browser.document
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = mainView
        }


init : () -> ( ChartModel, Cmd Msg )
init _ =
    ( initialChartModel, Cmd.none )


mainView : ChartModel -> Document Msg
mainView model =
    { title = "Dynamic charts"
    , body = [ view model
             , node "link" [ href "../assets/style/main-style.css", rel "stylesheet" ] []
             , node "link" [ href "../assets/style/dynamic-bar-chart.css", rel "stylesheet" ] []
             ]
    }


subscriptions : ChartModel -> Sub Msg
subscriptions model =
    Sub.batch
      [ if Transition.isComplete model.barChart.transition
        && Transition.isComplete model.horizontalBarChart.transition
        && Transition.isComplete model.pieChart.transition1
        && Transition.isComplete model.pieChart.transition2
        && Transition.isComplete model.curveChart.transitionX
        && Transition.isComplete model.curveChart.transitionWidth
        && Transition.isComplete model.areaChart.transition
        && Transition.isComplete model.dotChart.transition
        && Transition.isComplete model.waterfallChart.transition
        && Transition.isComplete model.doubleChart.transitionWidth
        && Transition.isComplete model.doubleChart.transitionX
        then
          Sub.none
      else
          Browser.Events.onAnimationFrameDelta (round >> Tick)
      ]
