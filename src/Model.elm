module Model exposing (..)

-- Elm packages --
import Transition exposing (Transition)



---------------------------------- Model ----------------------------------------


-- Main Model

type alias ChartModel =
    { chartsTabs : Int
    , width : Float
    , height : Float
    , data : List (List PrzTableCell)
    , displayData : List (List PrzTableCell)
    , settings : ChartSettingsModel
    , maxHeightScale : Float
    , minHeightScale : Float
    , hiddenRows : List Int
    , tmpTdValue : ((Int, Int),String)
    , tableRows : Int
    , tableColumns : Int
    , tableDropdown : (Int, Bool)
    , padding : ChartPadding
    , barChart : BarChartModel
    , horizontalBarChart : HorizontalBarChartModel
    , pieChart : PieChartModel
    , curveChart : CurveChartModel
    , areaChart : AreaChartModel
    , dotChart : DotChartModel
    , waterfallChart : WaterfallChartModel
    , doubleChart : DoubleChartModel
    }


-- Chart Model

type alias BarChartModel =
    { typeChart : String
    , transition : Transition Float
    , current_settingsTab : Int
    , stacked : Bool
    }

type alias HorizontalBarChartModel =
    { typeChart : String
    , transition : Transition Float
    , current_settingsTab : Int
    }

type alias PieChartModel =
    { typeChart : String
    , transition1 : Transition Float
    , transition2 : Transition Float
    , current_settingsTab : Int
    , colName : Int
    }

type alias CurveChartModel =
    { typeChart : String
    , transitionX : Transition Float
    , transitionWidth : Transition Float
    , current_settingsTab : Int
    , inLine : Bool
    }

type alias AreaChartModel =
    { typeChart : String
    , sortedData : List Int
    , transition : Transition Float
    , current_settingsTab : Int
    }


type alias DotChartModel =
    { typeChart : String
    , transition : Transition Float
    , current_settingsTab : Int
    , fillColor : String
    , borderColor : String
    }


type alias WaterfallChartModel =
    { typeChart : String
    , soldRows : List Int
    , maxHeight : Float
    , minHeight : Float
    , totalBarColor : String
    , positiveBarColor : String
    , negativeBarColor : String
    , lineColor : String
    , transition : Transition Float
    , current_settingsTab : Int
    , colIndex : Int
    }


type alias DoubleChartModel =
    { typeChart : String
    , maxBarHeight : Float
    , minBarHeight : Float
    , maxCurveHeight : Float
    , minCurveHeight : Float
    , transitionX : Transition Float
    , transitionWidth : Transition Float
    , current_settingsTab : Int
    , dataInCurve : List (List PrzTableCell)
    , dataInBar : List (List PrzTableCell)
    , indexList : (List String, List String)
    }


-- Settings Model
type alias ChartSettingsModel =
    { backgroundColor : String
    , animated : Bool
    , title : String
    , source : String
    , axeX : String
    , axeY : String
    , titleColor : String
    , titlePosition : String
    , sourcePosition : String
    , legendPosition : String
    , titlesPos : ( Float, Float )
    , sourcePos : ( Float, Float )
    , showTitle : Bool
    , showSource : Bool
    , showLegend : Bool
    , showAxes : Bool
    , showValueOnHover : Bool
    , globalFontFamily : String
    , titleTxtSettings : TextSettings
    , sourceTxtSettings : TextSettings
    , legendTxtSettings : TextSettings
    , valueTxtSettings : TextSettings
    , axeXTxtSettings : TextSettings
    , axeYTxtSettings : TextSettings
    , dataPrefix : String
    , dataSuffix : String
    , decimalSeparator : String
    , thousandsSeparator : String
    , simpleData : Bool
    }

type alias TextSettings =
  { fontFamily : String
  , fontSize : Int
  , fontColor : String
  , autoColor : Bool
  }

type alias ChartPadding =
    { bottom : Float, left : Float, right : Float, top : Float }

type alias PrzTableCell =
    { name : String, color : String }



----------------------------- Model Initialization -----------------------------


-- main model
initialChartModel : ChartModel
initialChartModel =
  { chartsTabs = 0
  , width = 800 --900
  , height = 300 --450
  , data = initialData
  , displayData = []
  , settings = initialChartSettings
  , maxHeightScale = 200
  , minHeightScale = 0
  , hiddenRows = []
  , tmpTdValue = ((0, 0), "")
  , tableRows = 20
  , tableColumns = 6
  , tableDropdown = (0, False)
  , padding = ChartPadding 50 50 50 50
  , barChart = initialBarChartModel
  , horizontalBarChart = initialHorizontalBarChartModel
  , pieChart = initialPieChart
  , curveChart = initialCurveChartModel
  , areaChart = initialAreaChart
  , dotChart = initialDotChart
  , waterfallChart = initialWaterfallChart
  , doubleChart = initialDoubleChart
  }


-- chart model

initialBarChartModel : BarChartModel
initialBarChartModel =
  { typeChart = "barChart"
  , transition = Transition.constant 10
  , current_settingsTab = 0
  , stacked = False
  }


initialHorizontalBarChartModel : HorizontalBarChartModel
initialHorizontalBarChartModel =
  { typeChart = "horizontalBarChart"
  , transition = Transition.constant 10
  , current_settingsTab = 0
  }


initialPieChart : PieChartModel
initialPieChart =
  { typeChart = "pieChart"
  , transition1 = Transition.constant 0
  , transition2 = Transition.constant 0
  , current_settingsTab = 0
  , colName = 0
  }


initialCurveChartModel : CurveChartModel
initialCurveChartModel =
    { typeChart = "curveChart"
    , transitionX = Transition.constant 0
    , transitionWidth = Transition.constant 0
    , current_settingsTab = 0
    , inLine = False
    }


initialAreaChart : AreaChartModel
initialAreaChart =
  { typeChart = "areaChart"
  , sortedData = []
  , transition = Transition.constant 0
  , current_settingsTab = 0
  }

initialDotChart : DotChartModel
initialDotChart =
  { typeChart = "areaChart"
  , transition = Transition.constant 0
  , current_settingsTab = 0
  , fillColor = "#FFFFFF"
  , borderColor = "#000000"
  }


initialWaterfallChart : WaterfallChartModel
initialWaterfallChart =
  { typeChart = "areaChart"
  , soldRows = []
  , maxHeight = 0
  , minHeight = 0
  , totalBarColor = "#A5A5A5"
  , positiveBarColor = "#C1F9BB"
  , negativeBarColor = "#F9BBBB"
  , lineColor = "#B5B5B5"
  , transition = Transition.constant 0
  , current_settingsTab = 0
  , colIndex = 0
  }


initialDoubleChart : DoubleChartModel
initialDoubleChart =
    { typeChart = "doubleChart"
    , maxBarHeight = 0
    , minBarHeight = 0
    , maxCurveHeight = 0
    , minCurveHeight = 0
    , transitionX = Transition.constant 0
    , transitionWidth = Transition.constant 0
    , current_settingsTab = 0
    , dataInCurve = []
    , dataInBar = []
    , indexList = ([],[])
    }


-- settings

initialChartSettings : ChartSettingsModel
initialChartSettings =
    { backgroundColor = "#FFFFFF"
    , animated = True
    , title = ""
    , source = ""
    , axeX = ""
    , axeY = ""
    , titleColor = "#000000"
    , titlePosition = "top left"
    , sourcePosition = "top left"
    , legendPosition = "bottom center"
    , titlesPos = ( 40, 40 )
    , sourcePos = ( 40, 40 + 30 )
    , showTitle = True
    , showSource = False
    , showLegend = True
    , showAxes = False
    , showValueOnHover = False
    , globalFontFamily = "none"
    , titleTxtSettings = initTitleSettings
    , sourceTxtSettings = initSourceSettings
    , legendTxtSettings = initLegendSettings
    , valueTxtSettings = initValueSettings
    , axeXTxtSettings = initAxeXSettings
    , axeYTxtSettings = initAxeYSettings
    , dataPrefix = ""
    , dataSuffix = ""
    , decimalSeparator = "."
    , thousandsSeparator = ","
    , simpleData = False
    }


initialTableHeader = PrzTableCell "" "#000000"
initialCell = PrzTableCell "" "#000000"

initFontFamily = "Arial"
initFontSize = 15
initTxtColor = "#000000"
initAutoColor = False


initTitleSettings : TextSettings
initTitleSettings =
  { fontFamily = initFontFamily
  , fontSize = 20
  , fontColor = "#000000"
  , autoColor = initAutoColor
  }

initSourceSettings : TextSettings
initSourceSettings =
  { fontFamily = initFontFamily
  , fontSize = 12
  , fontColor = initTxtColor
  , autoColor = initAutoColor
  }

initLegendSettings : TextSettings
initLegendSettings =
  { fontFamily = initFontFamily
  , fontSize = 14
  , fontColor = initTxtColor
  , autoColor = initAutoColor
  }

initValueSettings : TextSettings
initValueSettings =
  { fontFamily = initFontFamily
  , fontSize = 20
  , fontColor = initTxtColor
  , autoColor = initAutoColor
  }

initAxeXSettings : TextSettings
initAxeXSettings =
  { fontFamily = initFontFamily
  , fontSize = 10
  , fontColor = initTxtColor
  , autoColor = initAutoColor
  }

initAxeYSettings : TextSettings
initAxeYSettings =
  { fontFamily = initFontFamily
  , fontSize = 10
  , fontColor = initTxtColor
  , autoColor = initAutoColor
  }


-- data

initialData : List (List PrzTableCell)
initialData =
    [ [ PrzTableCell "" ""
      , PrzTableCell "Mars" "#7688FF"
      , PrzTableCell "Avril" "#FF0CA0"
      , PrzTableCell "Mai" "#FF0B00"
      , PrzTableCell "Juin" "#1F0F60"
      ]
    , [ PrzTableCell "LinkedIn" "#B6D0A4"
      , PrzTableCell "120" "#000000"
      , PrzTableCell "530" "#000000"
      , PrzTableCell "350" "#000000"
      , PrzTableCell "640" "#000000"
      ]
    , [ PrzTableCell "Facebook" "#FD8070"
      , PrzTableCell "300" "#000000"
      , PrzTableCell "260" "#000000"
      , PrzTableCell "-500" "#00000"
      , PrzTableCell "-230" "#000000"
      ]
    , [ PrzTableCell "MediaDesk" "#F3CE5F"
      , PrzTableCell "420" "#000000"
      , PrzTableCell "790" "#000000"
      , PrzTableCell "-150" "#000000"
      , PrzTableCell "410" "#000000"
      ]
    , [ PrzTableCell "Insta" "#A4D0CE"
      , PrzTableCell "-70" "#000000"
      , PrzTableCell "-300" "#000000"
      , PrzTableCell "200" "#000000"
      , PrzTableCell "90" "#000000"
      ]
    , [ PrzTableCell "whatsapp" "#D0A4A4"
      , PrzTableCell "350" "#000000"
      , PrzTableCell "490" "#000000"
      , PrzTableCell "50" "#000000"
      , PrzTableCell "500" "#000000"
      ]

    ]





fontFamilyList : List String
fontFamilyList =
  [ "Consolas"
  , "Courier"
  , "Lucida Console"
  , "Cambria Math"
  , "Segoe UI Symbol"
  , "Arial Black"
  , "Arial"
  , "Calibri"
  , "Cambria"
  , "Candara"
  , "Comic Sans MS"
  , "Constantia"
  , "Corbel"
  , "Georgia"
  , "MS Sans Serif"
  , "MS Serif"
  , "Segoe Print"
  , "Segoe Script"
  , "Times New Roman"
  , "Verdana"
  ]
